import {Component, Input, OnInit} from '@angular/core';
import {List} from '../../model/List';
import {Priority} from '../../model/Priority';
import {faFolder, faPen, faTimes} from '@fortawesome/free-solid-svg-icons';
import {Router} from '@angular/router';
import {AppService} from '../../providers/app.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss']
})
export class ListViewComponent implements OnInit {
  @Input() list: List;

  iconEdit = faPen;
  iconDelete = faTimes;
  faFolder = faFolder;

  constructor(private router: Router, private location: Location, private app: AppService) { }

  ngOnInit() {
  }

  edit(): void {
    this.router.navigate(['/edit', 'list', this.list.id]);
  }

  delete(): void {
    this.app.deleteList(this.list);
  }

  get priority(): string {
    return Priority[this.list.priority].toLowerCase();
  }
}
