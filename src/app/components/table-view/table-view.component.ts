import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {Item} from '../../model/Item';
import {List} from '../../model/List';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import {AppService} from '../../providers/app.service';
import {faPen, faPlus, faTimes} from '@fortawesome/free-solid-svg-icons';

export enum SortFields {
  Priority,
  Name,
  Budget,
  Price,
}

export enum SortOrders {
  Desc,
  Asc,
}

export interface Element {
  key: string;
  element: List | Item;
}

@Component({
  selector: 'app-table-view',
  templateUrl: './table-view.component.html',
  styleUrls: ['./table-view.component.scss']
})
export class TableViewComponent implements OnInit, OnChanges {
  @Input() list: List;
  @Input() lists: List[] = [];
  @Input() items: Item[] = [];

  filter = '';
  elements: Element[];

  SortFields = SortFields;

  selectedSortField = SortFields.Name;
  selectedSortOrder = SortOrders.Desc;

  iconAdd = faPlus;
  iconEdit = faPen;
  iconDelete = faTimes;

  constructor(private router: Router, private location: Location, private app: AppService) { }

  ngOnInit() {
    this.sort();
    this.app.change.subscribe(() => this.ngOnChanges());
  }

  ngOnChanges(): void {
    this.elements = [];
    this.items.forEach(i => this.elements.push({key: 'item', element: i}));
    this.lists.forEach(i => this.elements.push({key: 'list', element: i}));
    this.sort();
  }

  edit() {
    const args = ['/edit', 'list'];
    if (this.list) {
      args.push(this.list.id);
    }
    this.router.navigate(args);
  }

  newItem() {
    const args = ['/new', 'item'];
    if (this.list) {
      args.push(this.list.id);
    }
    this.router.navigate(args);
  }

  newList() {
    const args = ['/new', 'list'];
    if (this.list) {
      args.push(this.list.id);
    }
    this.router.navigate(args);
  }

  delete() {
    if (!this.list) {
      return;
    }

    this.app.deleteList(this.list);
    this.location.back();
  }

  sortByPriority(): void {
    this.sortByField(SortFields.Priority);
  }

  sortByName(): void {
    this.sortByField(SortFields.Name);
  }

  sortByBudget(): void {
    this.sortByField(SortFields.Budget);
  }

  sortByPrice(): void {
    this.sortByField(SortFields.Price);
  }

  private sort(): void {
    this.elements.sort((a: Element, b: Element) => {
      const ea = a.element;
      const eb = b.element;
      switch (this.selectedSortOrder) {
        case SortOrders.Desc:
          switch (this.selectedSortField) {
            case SortFields.Priority: return ea.priority < eb.priority ? -1 : 1;
            case SortFields.Name: return ea.name < eb.name ? -1 : 1;
            case SortFields.Budget: return ea.budget < eb.budget ? -1 : 1;
            case SortFields.Price: return ea.price < eb.price ? -1 : 1;
            default: return 0;
          }
        case SortOrders.Asc:
          switch (this.selectedSortField) {
            case SortFields.Priority: return ea.priority < eb.priority ? 1 : -1;
            case SortFields.Name: return ea.name < eb.name ? 1 : -1;
            case SortFields.Budget: return ea.budget < eb.budget ? 1 : -1;
            case SortFields.Price: return ea.price < eb.price ? 1 : -1;
            default: return 0;
          }
        default: return 0;
      }
    });
  }

  private sortByField(field: SortFields): void {
    if (this.selectedSortField === field) {
      this.nextSortOrder();
    } else {
      this.selectSortField(field);
      this.resetSortOrder();
    }

    this.sort();
  }

  private selectSortField(newSortField: SortFields): void {
    this.selectedSortField = newSortField;
  }

  private resetSortOrder(): SortOrders {
    this.selectedSortOrder = SortOrders.Desc;
    return this.selectedSortOrder;
  }

  private nextSortOrder(): SortOrders {
    this.selectedSortOrder += 1;
    this.selectedSortOrder %= Object.keys(SortOrders).length / 2;
    return this.selectedSortOrder;
  }
}
