import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SortFields, SortOrders} from '../table-view.component';
import {faArrowDown, faArrowUp} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-sort-indicator',
  templateUrl: './sort-indicator.component.html',
  styleUrls: ['./sort-indicator.component.scss']
})
export class SortIndicatorComponent implements OnInit {
  @Input() field: string;
  @Input() sortable: boolean;
  @Input() sortField: SortFields;
  @Input() selectedSortField: SortFields;
  @Input() selectedSortOrder: SortOrders;
  @Output() sort = new EventEmitter();

  SortOrders = SortOrders;
  SortFields = SortFields;

  iconDesc = faArrowDown;
  iconAsc = faArrowUp;

  constructor() { }

  ngOnInit() {
  }

  get sortOrderClassName(): string {
    switch (this.selectedSortOrder) {
      case SortOrders.Desc: return 'desc';
      case SortOrders.Asc: return 'asc';
      default: return 'nso';
    }
  }
}
