import {Component, Input, OnInit} from '@angular/core';
import {Item} from '../../model/Item';
import {faPen, faTimes} from '@fortawesome/free-solid-svg-icons';
import {Priority} from '../../model/Priority';
import {Router} from '@angular/router';
import {AppService} from '../../providers/app.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-item-view',
  templateUrl: './item-view.component.html',
  styleUrls: ['./item-view.component.scss']
})
export class ItemViewComponent implements OnInit {
  @Input() item: Item;
  @Input() listId: string;

  iconEdit = faPen;
  iconDelete = faTimes;

  constructor(private router: Router, private location: Location, private app: AppService) { }

  ngOnInit() {
  }

  edit(): void {
    this.router.navigate(['/edit', 'item', this.item.id]);
  }

  delete(): void {
    this.app.deleteItem(this.item);
  }

  get priority(): string {
    return Priority[this.item.priority].toLowerCase();
  }
}
