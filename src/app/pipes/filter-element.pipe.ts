import { Pipe, PipeTransform } from '@angular/core';
import {Element} from '../components/table-view/table-view.component';

@Pipe({
  name: 'filterElement'
})
export class FilterElementPipe implements PipeTransform {
  transform(value: Element[], name: string): Element[] {
    return name ? value.filter(l => l.element.name.toLocaleLowerCase().includes(name.toLocaleLowerCase())) : value;
  }
}
