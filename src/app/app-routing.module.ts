import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './pages/home/home.component';
import {ListComponent} from './pages/list/list.component';
import {ItemComponent} from './pages/item/item.component';
import {EditListComponent} from './pages/edit-list/edit-list.component';
import {EditItemComponent} from './pages/edit-item/edit-item.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'list/:id', component: ListComponent },
  { path: 'item/:id', component: ItemComponent },
  // New list
  { path: 'new/list', component: EditListComponent },
  // New list with parent list
  { path: 'new/list/:listId', component: EditListComponent },
  // Edit list
  { path: 'edit/list/:id', component: EditListComponent },
  // New item
  { path: 'new/item/:listId', component: EditItemComponent },
  // Edit item
  { path: 'edit/item/:id', component: EditItemComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
