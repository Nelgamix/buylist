import {Priority} from './Priority';
import {Link} from './Link';
import {Tag} from './Tag';

export interface Element {
  id: string;
  image: string;
  name: string;
  description: string;
  note: string;
  budget: number;
  priority: Priority;
  tags: Tag[];
  links: Link[];
  created: number;
  updated: number;
}
