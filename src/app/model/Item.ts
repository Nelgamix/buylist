import {Priority} from './Priority';
import {Link} from './Link';
import {Chance} from 'chance';
import {Element} from './Element';
import {Tag} from './Tag';
import {cloneDeep} from 'lodash';

const chance = new Chance();

export interface IItem extends Element {
  price: number;
  unlock: number; // First date available (e.g release date of a game)
  lock: number; // Last date available (e.g limited merch)
  bought: boolean;
  boughtDate: number;
}

export class Item implements Element {
  static fromInterface(item: IItem): Item {
    return new Item(
      item.image,
      item.name,
      item.description,
      item.tags,
      item.note,
      item.price,
      item.budget,
      item.links,
      item.unlock,
      item.lock,
      item.priority,
      item.created,
      item.updated,
      item.bought,
      item.boughtDate,
      item.id,
    );
  }

  constructor(
    public image: string = '',
    public name: string = '',
    public description: string = '',
    public tags: Tag[] = [],
    public note: string = '',
    public price: number = 0,
    public budget: number = price,
    public links: Link[] = [],
    public unlock: number = 0,
    public lock: number = 0,
    public priority: Priority = Priority.Low,
    public created: number = Date.now(),
    public updated: number = Date.now(),
    public bought: boolean = false,
    public boughtDate: number = 0,
    public id: string = chance.string({length: 5, pool: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'}),
  ) {
  }

  toInterface(): IItem {
    return {
      id: this.id,
      image: this.image,
      name: this.name,
      description: this.description,
      tags: this.tags,
      note: this.note,
      price: this.price,
      budget: this.budget,
      links: this.links,
      unlock: this.unlock,
      lock: this.lock,
      priority: this.priority,
      created: this.created,
      updated: this.updated,
      bought: this.bought,
      boughtDate: this.boughtDate,
    };
  }

  clone(): Item {
    const item = new Item();
    item.copyFrom(this);
    return item;
  }

  copyFrom(item: Item): void {
    this.id = item.id;
    this.image = item.image;
    this.name = item.name;
    this.description = item.description;
    this.tags = cloneDeep(item.tags);
    this.note = item.note;
    this.price = item.price;
    this.budget = item.budget;
    this.links = cloneDeep(item.links);
    this.unlock = item.unlock;
    this.lock = item.lock;
    this.priority = item.priority;
    this.created = item.created;
    this.updated = item.updated;
    this.bought = item.bought;
    this.boughtDate = item.boughtDate;
  }

  copyTo(item: Item): void {
    item.copyFrom(this);
  }
}
