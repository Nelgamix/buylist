import {IItem, Item} from './Item';
import {Priority} from './Priority';
import {Chance} from 'chance';
import {Aggregate} from './Aggregate';
import {remove, cloneDeep} from 'lodash';
import {Element} from './Element';
import {Tag} from './Tag';
import {Link} from './Link';

const chance = new Chance();

export interface IList extends Element {
  aggregate: Aggregate;
  items: IItem[];
  lists: IList[];
}

export class List implements IList {
  static fromInterface(list: IList): List {
    return new List(
      list.image,
      list.name,
      list.description,
      list.note,
      list.tags,
      list.links,
      list.budget,
      list.priority,
      list.aggregate,
      list.created,
      list.updated,
      list.items.map(item => Item.fromInterface(item)),
      list.lists.map(l => List.fromInterface(l)),
      list.id,
    );
  }

  constructor(
    public image: string = '',
    public name: string = '',
    public description: string = '',
    public note: string = '',
    public tags: Tag[] = [],
    public links: Link[] = [],
    public budget: number = 0,
    public priority: Priority = Priority.Low,
    public aggregate: Aggregate = Aggregate.And,
    public created: number = Date.now(),
    public updated: number = Date.now(),
    public items: Item[] = [],
    public lists: List[] = [],
    public id: string = chance.string({length: 5, pool: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'}),
  ) {
  }

  toInterface(): IList {
    return {
      id: this.id,
      image: this.image,
      name: this.name,
      description: this.description,
      note: this.note,
      tags: this.tags,
      links: this.links,
      budget: this.budget,
      priority: this.priority,
      aggregate: this.aggregate,
      created: this.created,
      updated: this.updated,
      items: this.items.map(item => item.toInterface()),
      lists: this.lists.map(l => l.toInterface()),
    };
  }

  clone(): List {
    const list = new List();
    list.copyFrom(this);
    return list;
  }

  copyFrom(list: List): void {
    this.id = list.id;
    this.image = list.image;
    this.name = list.name;
    this.description = list.description;
    this.note = list.note;
    this.tags = cloneDeep(list.tags);
    this.links = cloneDeep(list.links);
    this.budget = list.budget;
    this.priority = list.priority;
    this.aggregate = list.aggregate;
    this.created = list.created;
    this.updated = list.updated;
    this.items = list.items.map(i => i.clone());
    this.lists = list.lists.map(l => l.clone());
  }

  copyTo(list: List): void {
    list.copyFrom(this);
  }

  deleteList(list: List): void {
    remove(this.lists, l => l.id === list.id);
    // Deep delete
    this.lists.forEach(l => l.deleteList(list));
  }

  deleteItem(item: Item): void {
    remove(this.items, i => i.id === item.id);
    // Deep delete
    this.lists.forEach(l => l.deleteItem(item));
  }

  get price(): number {
    return this.items.reduce((a, i) => a + i.price, 0);
  }

  get customBudget(): number {
    return this.items.reduce((a, i) => a + i.budget, 0);
  }

  get maxPrice(): number {
    return this.items.reduce((a, i) => Math.max(a, i.price), 0);
  }

  get minPrice(): number {
    return this.items.reduce((a, i) => Math.min(a, i.price), Infinity);
  }
}
