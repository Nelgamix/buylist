import {IList, List} from './List';
import {Chance} from 'chance';
import {remove} from 'lodash';
import {Item} from './Item';

const chance = new Chance();

export class IFolder {
  id: string;
  created: number;
  updated: number;
  lists: IList[];
}

export class Folder {
  static fromInterface(folder: IFolder): Folder {
    return new Folder(
      folder.created,
      folder.updated,
      folder.lists.map(l => List.fromInterface(l)),
      folder.id,
    );
  }

  constructor(
    public created: number = Date.now(),
    public updated: number = Date.now(),
    public lists: List[] = [],
    public id: string = chance.string({length: 5, pool: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'})
  ) {
  }

  toInterface(): IFolder {
    return {
      id: this.id,
      created: this.created,
      updated: this.updated,
      lists: this.lists.map(l => l.toInterface()),
    };
  }

  clone(): Folder {
    const folder = new Folder();
    folder.copyFrom(this);
    return folder;
  }

  copyFrom(folder: Folder): void {
    this.id = folder.id;
    this.created = folder.created;
    this.updated = folder.updated;
    this.lists = folder.lists.map(l => l.clone());
  }

  copyTo(folder: Folder): void {
    folder.copyFrom(this);
  }

  deleteList(list: List): void {
    remove(this.lists, l => l.id === list.id);
    this.lists.forEach(l => l.deleteList(list));
  }

  deleteItem(item: Item): void {
    this.lists.forEach(l => l.deleteItem(item));
  }
}
