export enum Priority {
  Low,
  Normal,
  High,
  Severe,
  Critical,
}

export function getPriorityKeys(): string[] {
  return Object.keys(Priority).filter(k => isNaN(+k));
}
