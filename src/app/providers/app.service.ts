import {Injectable} from '@angular/core';
import {List} from '../model/List';
import {Item} from '../model/Item';
import {Observable, of, Subject} from 'rxjs';
import {Priority} from '../model/Priority';
import {Folder, IFolder} from '../model/Folder';
import {Aggregate} from '../model/Aggregate';
import {mapTo} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  private _ready: Promise<void>;
  private resolveReady;

  private folder: Folder;
  private lists: Map<string, List>;
  private items: Map<string, Item>;
  private _change: Subject<void>;

  constructor() {
    this.folder = new Folder();
    this.lists = new Map();
    this.items = new Map();
    this._ready = new Promise(resolve => this.resolveReady = resolve);
    this._change = new Subject();

    // Dev mock
    // this.loadFolder(mockData);
    // this.resolveReady();
    // Save
    this.load().subscribe((res) => {
      if (res) {
        this.loadFolder(res);
      } else {
        this.folder = new Folder();
      }

      this.resolveReady();
    });
  }

  loadFolder(folder: IFolder): Observable<void> {
    this.folder = Folder.fromInterface(folder);
    this.folder.lists.forEach(l => this.loadFolderListsItems(l));
    return of();
  }

  getFolder(): Observable<Folder> {
    return of(this.folder);
  }

  getList(id: string): Observable<List> {
    return of(this.lists.get(id));
  }

  getItem(id: string): Observable<Item> {
    return of(this.items.get(id));
  }

  addOrUpdateList(list: List, parentId?: string): Observable<List> {
    const cached = this.lists.get(list.id);
    const parent = parentId ? this.lists.get(parentId) : this.folder;

    if (!cached && !parent) {
      return of(undefined);
    }

    if (cached) {
      // Update
      cached.copyFrom(list);
    } else {
      // Set
      this.lists.set(list.id, list);
    }

    if (parent && !parent.lists.find(l => l.id === list.id)) {
      parent.lists.push(list);
    }

    this._change.next();

    return this.save().pipe(mapTo(list));
  }

  addOrUpdateItem(item: Item, listId?: string): Observable<Item> {
    const cached = this.items.get(item.id);
    const list = listId ? this.lists.get(listId) : undefined;

    if (!cached && !list) {
      return of(undefined);
    }

    if (cached) {
      // Update
      cached.copyFrom(item);
    } else {
      // Set
      this.items.set(item.id, item);
    }

    if (list && !list.items.find(i => i.id === item.id)) {
      list.items.push(item);
    }

    this._change.next();

    this.save();

    return this.save().pipe(mapTo(item));
  }

  deleteList(list: List): Observable<void> {
    this.lists.delete(list.id);
    this.folder.deleteList(list);
    this._change.next();
    return this.save();
  }

  deleteItem(item: Item): Observable<void> {
    this.folder.deleteItem(item);
    this._change.next();
    return this.save();
  }

  save(): Observable<void> {
    localStorage.setItem('save', JSON.stringify(this.folder.toInterface()));
    return of();
  }

  load(): Observable<IFolder> {
    const sd = localStorage.getItem('save');

    if (!sd) {
      return of(undefined);
    }

    return of(JSON.parse(sd));
  }

  get ready(): Promise<void> {
    return this._ready;
  }

  get change(): Subject<void> {
    return this._change;
  }

  private loadFolderListsItems(list: List): void {
    this.lists.set(list.id, list);
    list.items.forEach(i => this.items.set(i.id, i));
    list.lists.forEach(l => this.loadFolderListsItems(l));
  }
}

const dateNow = Date.now();
const mockData: IFolder = {
  id: 'aaaaa',
  created: dateNow,
  updated: dateNow,
  lists: [
    {
      id: 'aaaaa',
      name: 'New PC build',
      description: 'The new PC to build later this year',
      note: 'This should be a very high end PC, let\'s see if we can get some pieces on sale.',
      tags: [
        {
          name: 'Hardware',
          description: ''
        }
      ],
      links: [],
      image: '',
      budget: 10,
      priority: Priority.Normal,
      aggregate: Aggregate.And,
      created: dateNow,
      updated: dateNow,
      items: [
        {
          id: 'aaaaa',
          image: 'https://www.nvidia.com/content/dam/en-zz/Solutions/geforce/geforce-rtx-turing/tech-shots/geforce-rtx-2080-ti-web-tech-shot-630-u@2x.png',
          name: 'RTX 2080 Ti',
          description: 'GPU',
          note: '',
          tags: [
            {
              name: 'Hardware',
              description: ''
            }
          ],
          price: 1200,
          budget: 1000,
          created: dateNow,
          updated: dateNow,
          lock: 0,
          unlock: 0,
          bought: false,
          boughtDate: 0,
          links: [],
          priority: Priority.High,
        }
      ],
      lists: [],
    }
  ],
};
