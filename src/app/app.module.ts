import 'reflect-metadata';
import '../polyfills';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { HttpClientModule, HttpClient } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

// NG Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { ElectronService } from './providers/electron.service';

import { WebviewDirective } from './directives/webview.directive';

import { AppComponent } from './app.component';

import {HomeComponent} from './pages/home/home.component';
import {ListComponent} from './pages/list/list.component';
import {ItemComponent} from './pages/item/item.component';
import {ListViewComponent} from './components/list-view/list-view.component';
import {EditListComponent} from './pages/edit-list/edit-list.component';
import {EditItemComponent} from './pages/edit-item/edit-item.component';
import {ItemViewComponent} from './components/item-view/item-view.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import { TableViewComponent } from './components/table-view/table-view.component';
import { SortIndicatorComponent } from './components/table-view/sort-indicator/sort-indicator.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule, MatChipsModule, MatInputModule, MatSelectModule} from '@angular/material';
import { FilterElementPipe } from './pipes/filter-element.pipe';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    WebviewDirective,
    ListComponent,
    ItemComponent,
    ListViewComponent,
    EditListComponent,
    EditItemComponent,
    ItemViewComponent,
    TableViewComponent,
    SortIndicatorComponent,
    FilterElementPipe,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    FontAwesomeModule,
    // Angular Material
    MatButtonModule,
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule,
    MatChipsModule,
    // NgxTranslate
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),
  ],
  providers: [ElectronService],
  bootstrap: [AppComponent]
})
export class AppModule { }
