import { Component, OnInit } from '@angular/core';
import {AppService} from '../../providers/app.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Item} from '../../model/Item';
import {Location} from '@angular/common';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {
  item: Item;

  constructor(
    private app: AppService,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      const id = params.id;
      this.app.ready.then(() => {
        this.app.getItem(id).subscribe((item) => {
          this.item = item;
        });
      });
    });
  }

  back() {
    this.location.back();
  }

  edit() {
    this.router.navigate(['/edit', 'item', this.item.id]);
  }

  delete() {
    this.app.deleteItem(this.item);
    this.location.back();
  }
}
