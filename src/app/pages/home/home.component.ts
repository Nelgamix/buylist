import { Component, OnInit } from '@angular/core';
import {AppService} from '../../providers/app.service';
import {Folder} from '../../model/Folder';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  folder: Folder;
  filter = '';

  constructor(private app: AppService, private router: Router) { }

  ngOnInit() {
    this.app.ready.then(() => {
      this.app.getFolder().subscribe((folder) => this.folder = folder);
    });
  }

  newList(): void {
    this.router.navigate(['/new', 'list']);
  }
}
