import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AppService} from '../../providers/app.service';
import {List} from '../../model/List';
import {getPriorityKeys, Priority} from '../../model/Priority';
import {Location} from '@angular/common';
import {Aggregate} from '../../model/Aggregate';
import {MatChipInputEvent} from '@angular/material';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {faTimesCircle} from '@fortawesome/free-solid-svg-icons';
import {Tag} from '../../model/Tag';

@Component({
  selector: 'app-edit-list',
  templateUrl: './edit-list.component.html',
  styleUrls: ['./edit-list.component.scss']
})
export class EditListComponent implements OnInit {
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  faTimesCircle = faTimesCircle;

  list: List;
  listId: string;

  priorities = getPriorityKeys();
  Priority = Priority;
  Aggregate = Aggregate;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private app: AppService
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.listId = params.listId;
      const id = params.id;

      this.app.ready.then(() => {
        if (id) {
          // Edit existing item!
          this.app.getList(id).subscribe(list => {
            this.list = list;
          });
        } else {
          this.list = new List();
        }
      });
    });
  }

  addTag(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.list.tags.push({name: value.trim(), description: ''});
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  removeTag(tag: Tag): void {
    const index = this.list.tags.indexOf(tag);

    if (index >= 0) {
      this.list.tags.splice(index, 1);
    }
  }

  cancel(): void {
    this.location.back();
  }

  ok(): void {
    this.app.addOrUpdateList(this.list, this.listId);
    this.location.back();
  }
}
