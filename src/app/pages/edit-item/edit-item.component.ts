import { Component, OnInit } from '@angular/core';
import {getPriorityKeys, Priority} from '../../model/Priority';
import {ActivatedRoute, Router} from '@angular/router';
import {AppService} from '../../providers/app.service';
import {Item} from '../../model/Item';
import {Location} from '@angular/common';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatChipInputEvent} from '@angular/material';
import {faTimesCircle} from '@fortawesome/free-solid-svg-icons';
import {Tag} from '../../model/Tag';

@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.component.html',
  styleUrls: ['./edit-item.component.scss']
})
export class EditItemComponent implements OnInit {
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  faTimesCircle = faTimesCircle;

  item: Item;
  listId: string;

  priorities = getPriorityKeys();
  Priority = Priority;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private app: AppService
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.listId = params.listId;
      const id = params.id;

      this.app.ready.then(() => {
        if (id) {
          this.app.getItem(id).subscribe(item => {
            this.item = item.clone();
          });
        } else {
          this.item = new Item();
        }
      });
    });
  }

  addTag(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.item.tags.push({name: value.trim(), description: ''});
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  removeTag(tag: Tag): void {
    const index = this.item.tags.indexOf(tag);

    if (index >= 0) {
      this.item.tags.splice(index, 1);
    }
  }

  cancel(): void {
    this.location.back();
  }

  ok(): void {
    this.app.addOrUpdateItem(this.item, this.listId);
    this.location.back();
  }
}
