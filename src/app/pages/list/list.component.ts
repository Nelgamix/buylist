import { Component, OnInit } from '@angular/core';
import {AppService} from '../../providers/app.service';
import {List} from '../../model/List';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {Aggregate} from '../../model/Aggregate';
import {faArrowUp} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  list: List;
  filter = '';

  Aggregate = Aggregate;

  iconUp = faArrowUp;

  constructor(
    private app: AppService,
    private route: ActivatedRoute,
    private location: Location
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      const id = params.id;
      this.app.ready.then(() => {
        this.app.getList(id).subscribe((list) => {
          this.list = list;
        });
      });
    });
  }

  back() {
    this.location.back();
  }
}
